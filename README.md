# hdfs-test-env
Docker based test environment for webHDFS.


* build the image

```
./build.sh
```

* run (-> in interactive mode, with a bash shell)

```
./run.sh
```

Check `test.sh` how to store/get data to/from HDFS (with `curl`)
