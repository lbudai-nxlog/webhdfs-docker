# WebHDFS API

### getfilestatus

* request to namenode

```
curl -i -X GET -L "http://hadoop:9870/webhdfs/v1/test.txt?user.name=root&op=GETFILESTATUS"
```

* response

```
HTTP/1.1 404 Not Found
Date: Fri, 06 Aug 2021 09:19:35 GMT
Cache-Control: no-cache
Expires: Fri, 06 Aug 2021 09:19:36 GMT
Date: Fri, 06 Aug 2021 09:19:36 GMT
Pragma: no-cache
X-Content-Type-Options: nosniff
X-FRAME-OPTIONS: SAMEORIGIN
X-XSS-Protection: 1; mode=block
Set-Cookie: hadoop.auth="u=root&p=root&t=simple&e=1628277576252&s=AgJJMSNeQl6OkAVoP+2Sjcqsub8uZcHo43ah2IGyZ2g="; Path=/; HttpOnly
Content-Type: application/json
Transfer-Encoding: chunked

{"RemoteException":{"exception":"FileNotFoundException","javaClassName":"java.io.FileNotFoundException","message":"File does not exist: /test.txt"}}% 


```

### create

* note: data is sent only to datanode!

#### create without data

* initial request to namenode

```
curl -i -X PUT -L "http://hadoop:9870/webhdfs/v1/test.txt?user.name=root&op=CREATE"                                                                      
```

* messages

Note
 * HTTP 307 : request is redirected to a datanode,
 * HTTP 100 : continue
 * HTTP 201 : created (Connection: close)
    * Location!


```
HTTP/1.1 307 Temporary Redirect
Date: Fri, 06 Aug 2021 09:21:59 GMT
Cache-Control: no-cache
Expires: Fri, 06 Aug 2021 09:21:59 GMT
Date: Fri, 06 Aug 2021 09:21:59 GMT
Pragma: no-cache
X-Content-Type-Options: nosniff
X-FRAME-OPTIONS: SAMEORIGIN
X-XSS-Protection: 1; mode=block
Set-Cookie: hadoop.auth="u=root&p=root&t=simple&e=1628277719322&s=P4qVG1DId0kFlghkbkc4JtPom0iO67/dy+tqyxdm0Kw="; Path=/; HttpOnly
Location: http://hadoop:9864/webhdfs/v1/test.txt?op=CREATE&user.name=root&namenoderpcaddress=172.17.0.2:9000&createflag=&createparent=true&overwrite=false
Content-Type: application/octet-stream
Content-Length: 0

HTTP/1.1 100 Continue

HTTP/1.1 201 Created
Location: hdfs://172.17.0.2:9000/test.txt
Content-Length: 0
Access-Control-Allow-Origin: *
Connection: close

```


#### create with data

* initial request to namenode

```
curl -i -X PUT -L "http://hadoop:9870/webhdfs/v1/test2.txt?user.name=root&op=CREATE" -T testx.txt 
```

* messages

```
HTTP/1.1 100 Continue

HTTP/1.1 307 Temporary Redirect
Date: Fri, 06 Aug 2021 09:24:27 GMT
Cache-Control: no-cache
Expires: Fri, 06 Aug 2021 09:24:27 GMT
Date: Fri, 06 Aug 2021 09:24:27 GMT
Pragma: no-cache
X-Content-Type-Options: nosniff
X-FRAME-OPTIONS: SAMEORIGIN
X-XSS-Protection: 1; mode=block
Set-Cookie: hadoop.auth="u=root&p=root&t=simple&e=1628277867929&s=O1ZIiRXtq0Cd9ger/cxUmnRmH4SFLnr37jhoivOB6rA="; Path=/; HttpOnly
Location: http://hadoop:9864/webhdfs/v1/test2.txt?op=CREATE&user.name=root&namenoderpcaddress=172.17.0.2:9000&createflag=&createparent=true&overwrite=false
Content-Type: application/octet-stream
Content-Length: 0

HTTP/1.1 100 Continue

HTTP/1.1 201 Created
Location: hdfs://172.17.0.2:9000/test2.txt
Content-Length: 0
Access-Control-Allow-Origin: *
Connection: close

```

### get file status after file is created

* initial request to namenode

```
curl -i -X GET -L "http://hadoop:9870/webhdfs/v1/test.txt?user.name=root&op=GETFILESTATUS"

```

* response

```
HTTP/1.1 200 OK
Date: Fri, 06 Aug 2021 09:28:23 GMT
Cache-Control: no-cache
Expires: Fri, 06 Aug 2021 09:28:23 GMT
Date: Fri, 06 Aug 2021 09:28:23 GMT
Pragma: no-cache
X-Content-Type-Options: nosniff
X-FRAME-OPTIONS: SAMEORIGIN
X-XSS-Protection: 1; mode=block
Set-Cookie: hadoop.auth="u=root&p=root&t=simple&e=1628278103270&s=F9Lc09mKf6QsJCTk4Ath81HxkGH8uROHUjtsGXBxrdk="; Path=/; HttpOnly
Content-Type: application/json
Transfer-Encoding: chunked

{"FileStatus":{"accessTime":1628241719573,"blockSize":134217728,"childrenNum":0,"fileId":16386,"group":"supergroup","length":0,"modificationTime":1628241719617,"owner":"root","pathSuffix":"","permission":"644","replication":1,"storagePolicy":0,"type":"FILE"}}%
```

### append data to file

* initial request to namenode

```
curl -i -X POST -L "http://hadoop:9870/webhdfs/v1/test.txt?user.name=root&op=APPEND&" -T testx.txt
```

* messages

```
HTTP/1.1 100 Continue

HTTP/1.1 307 Temporary Redirect
Date: Fri, 06 Aug 2021 09:30:10 GMT
Cache-Control: no-cache
Expires: Fri, 06 Aug 2021 09:30:10 GMT
Date: Fri, 06 Aug 2021 09:30:10 GMT
Pragma: no-cache
X-Content-Type-Options: nosniff
X-FRAME-OPTIONS: SAMEORIGIN
X-XSS-Protection: 1; mode=block
Set-Cookie: hadoop.auth="u=root&p=root&t=simple&e=1628278210244&s=vroZ+Ylg5b7FtMlip7LyJvGjLID0PHGJK/kuUAoVcv4="; Path=/; HttpOnly
Location: http://hadoop:9864/webhdfs/v1/test.txt?op=APPEND&user.name=root&namenoderpcaddress=172.17.0.2:9000
Content-Type: application/octet-stream
Content-Length: 0

HTTP/1.1 100 Continue

HTTP/1.1 200 OK
Content-Length: 0
Connection: close

```

### get content

* initial request to namenode

```
curl -i -X GET -L "http://hadoop:9870/webhdfs/v1/test.txt?user.name=root&op=OPEN"
```

* messages

```
HTTP/1.1 307 Temporary Redirect
Date: Fri, 06 Aug 2021 09:31:15 GMT
Cache-Control: no-cache
Expires: Fri, 06 Aug 2021 09:31:15 GMT
Date: Fri, 06 Aug 2021 09:31:15 GMT
Pragma: no-cache
X-Content-Type-Options: nosniff
X-FRAME-OPTIONS: SAMEORIGIN
X-XSS-Protection: 1; mode=block
Set-Cookie: hadoop.auth="u=root&p=root&t=simple&e=1628278275907&s=YrpxAb+jVBK0qNjolf2koAOmVpQoiwv9yEBlv98wmQ0="; Path=/; HttpOnly
Location: http://hadoop:9864/webhdfs/v1/test.txt?op=OPEN&user.name=root&namenoderpcaddress=172.17.0.2:9000&offset=0
Content-Type: application/octet-stream
Content-Length: 0

HTTP/1.1 200 OK
Access-Control-Allow-Methods: GET
Access-Control-Allow-Origin: *
Content-Type: application/octet-stream
Connection: close
Content-Length: 8

xxx
xxx

```

### APPEND when file does not exists

* request

```
curl -i -X POST -L "http://hadoop:9870/webhdfs/v1/testX.txt?user.name=root&op=APPEND&" -T testx.txt
```

* response

```
HTTP/1.1 100 Continue

HTTP/1.1 404 Not Found
Date: Fri, 06 Aug 2021 09:45:12 GMT
Cache-Control: no-cache
Expires: Fri, 06 Aug 2021 09:45:12 GMT
Date: Fri, 06 Aug 2021 09:45:12 GMT
Pragma: no-cache
X-Content-Type-Options: nosniff
X-FRAME-OPTIONS: SAMEORIGIN
X-XSS-Protection: 1; mode=block
Set-Cookie: hadoop.auth="u=root&p=root&t=simple&e=1628279112028&s=feX6dHGohdaBkORAkWnRI+2oTWxVpf0OChaHSWiI6A4="; Path=/; HttpOnly
Content-Type: application/json
Transfer-Encoding: chunked

{"RemoteException":{"exception":"FileNotFoundException","javaClassName":"java.io.FileNotFoundException","message":"File /testX.txt not found."}}%
```

### create when file already exists

* request

```
curl -i -X PUT -L "http://hadoop:9870/webhdfs/v1/test2.txt?user.name=root&op=CREATE" -T testx.txt
```

* messages

```
HTTP/1.1 100 Continue

HTTP/1.1 307 Temporary Redirect
Date: Fri, 06 Aug 2021 09:48:22 GMT
Cache-Control: no-cache
Expires: Fri, 06 Aug 2021 09:48:22 GMT
Date: Fri, 06 Aug 2021 09:48:22 GMT
Pragma: no-cache
X-Content-Type-Options: nosniff
X-FRAME-OPTIONS: SAMEORIGIN
X-XSS-Protection: 1; mode=block
Set-Cookie: hadoop.auth="u=root&p=root&t=simple&e=1628279302230&s=WT85xPIIgiIsT8Y1eOWT4nwchv7V1UhUP0mNbjyC0P0="; Path=/; HttpOnly
Location: http://hadoop:9864/webhdfs/v1/test2.txt?op=CREATE&user.name=root&namenoderpcaddress=172.17.0.2:9000&createflag=&createparent=true&overwrite=false
Content-Type: application/octet-stream
Content-Length: 0

HTTP/1.1 100 Continue

HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8
Content-Length: 1681
Connection: close

{"RemoteException":{"exception":"FileAlreadyExistsException","javaClassName":"org.apache.hadoop.fs.FileAlreadyExistsException","message":"/test2.txt for client 172.17.0.2 already exists\n\tat org.apache.hadoop.hdfs.server.namenode.FSDirWriteFileOp.startFile(FSDirWriteFileOp.java:389)\n\tat org.apache.hadoop.hdfs.server.namenode.FSNamesystem.startFileInt(FSNamesystem.java:2685)\n\tat org.apache.hadoop.hdfs.server.namenode.FSNamesystem.startFile(FSNamesystem.java:2578)\n\tat org.apache.hadoop.hdfs.server.namenode.NameNodeRpcServer.create(NameNodeRpcServer.java:810)\n\tat org.apache.hadoop.hdfs.protocolPB.ClientNamenodeProtocolServerSideTranslatorPB.create(ClientNamenodeProtocolServerSideTranslatorPB.java:494)\n\tat org.apache.hadoop.hdfs.protocol.proto.ClientNamenodeProtocolProtos$ClientNamenodeProtocol$2.callBlockingMethod(ClientNamenodeProtocolProtos.java)\n\tat org.apache.hadoop.ipc.ProtobufRpcEngine2$Server$ProtoBufRpcInvoker.call(ProtobufRpcEngine2.java:600)\n\tat org.apache.hadoop.ipc.ProtobufRpcEngine2$Server$ProtoBufRpcInvoker.call(ProtobufRpcEngine2.java:568)\n\tat org.apache.hadoop.ipc.ProtobufRpcEngine2$Server$ProtoBufRpcInvoker.call(ProtobufRpcEngine2.java:552)\n\tat org.apache.hadoop.ipc.RPC$Server.call(RPC.java:1093)\n\tat org.apache.hadoop.ipc.Server$RpcCall.run(Server.java:1035)\n\tat org.apache.hadoop.ipc.Server$RpcCall.run(Server.java:963)\n\tat java.base/java.security.AccessController.doPrivileged(Native Method)\n\tat java.base/javax.security.auth.Subject.doAs(Subject.java:423)\n\tat org.apache.hadoop.security.UserGroupInformation.doAs(UserGroupInformation.java:1878)\n\tat org.apache.hadoop.ipc.Server$Handler.run(Serve)
```

### Delete a file

* request

```
 cul -i -X DELETE -L "http://hadoop:9870/webhdfs/v1/test.txt?user.name=root&op=DELETE"
```

* response

```
 HTTP/1.1 200 OK
 Date: Fri, 13 Aug 2021 09:52:39 GMT
 Cache-Control: no-cache
 Expires: Fri, 13 Aug 2021 09:52:39 GMT
 Date: Fri, 13 Aug 2021 09:52:39 GMT
 Pragma: no-cache
 X-Content-Type-Options: nosniff
 X-FRAME-OPTIONS: SAMEORIGIN
 X-XSS-Protection: 1; mode=block
 Set-Cookie: hadoop.auth="u=root&p=root&t=simple&e=1628884359466&s=X8We7heIdC8/eO/E0l2VwCQLDFEu59sVlKuAOg+EjQU="; Path=/; HttpOnly
 Content-Type: application/json
 Transfer-Encoding: chunked

```

